package fr.cylpios.kryptage_one;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.security.PublicKey;
import java.util.List;

public class KryptageUserList extends ArrayAdapter<KryptageUser> {

    public Activity context;
    public List<KryptageUser> kuserList;

    public KryptageUserList(Activity context, List<KryptageUser> kuserList){
        super(context, R.layout.list_item, kuserList);
        this.context = context;
        this.kuserList = kuserList;
        Log.d("Action", "Initiate Inflater");

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        Log.d("Action", "Create LISTVIEW");

        View listViewItem = inflater.inflate(R.layout.list_item, null, true);
        TextView textViewIP = (TextView) listViewItem.findViewById(R.id.textViewIP);
        TextView textViewCode = (TextView) listViewItem.findViewById(R.id.textViewCode);
        TextView textViewTime = (TextView) listViewItem.findViewById(R.id.textViewTime);

        ImageView imageViewValide = (ImageView) listViewItem.findViewById((R.id.imageViewValide));

        KryptageUser kuser = kuserList.get(position);

        textViewIP.setText(kuser.getIp());
        textViewCode.setText(String.valueOf(kuser.getCode()));

        String time = kuser.getTime();
        if (time != null && time.length() > 0) {
            time = time.substring(0, 23);
        }

        textViewTime.setText(time);

        int imageSource = R.mipmap.ic_cookie_0;
        try{
            if(kuser.getValide()){
                imageSource = R.mipmap.ic_cookie_1;
            }
        }catch (Exception e){
            Log.d("Error", String.valueOf(e));
            imageSource = R.mipmap.ic_cookie_e;

        }
        imageViewValide.setImageResource(imageSource);
        return listViewItem;
    }

}
