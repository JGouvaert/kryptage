package fr.cylpios.kryptage_one;
import android.support.annotation.NonNull;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class KryptageUserValidator {
    public String nom;
    public String prenom;
    public String section;

    public KryptageUserValidator(){
        //Default constructor for calls to Databse
    }

    public KryptageUserValidator(String nom, String prenom, String section){
        this.nom = nom;
        this.prenom = prenom;
        this.section = section;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    @NonNull
    public String getPrenom() {
        return prenom;
    }

    @NonNull
    public String getSection() {
        return section;
    }
}
