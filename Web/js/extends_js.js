var ip = "",
date = new Date(),
code;

String.prototype.hashCode = function(){
	var hash = undefined;
	if (this.length == 0) return hash;
	for (i = 0; i < this.length; i++) {
		char = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}

function center(elmt, random){
  var imageWidth = elmt.offsetWidth,
      imageHeight = elmt.offsetHeight,
      vpWidth = document.documentElement.clientWidth,
      vpHeight = document.documentElement.clientHeight,
			leftR = 0,
			topR = 0;

	if(random){
		var rint = Math.floor(Math.random() * imageWidth/2);
	  // Left
	      leftR = rint * (Math.random() < 0.5 ? -1 : 1);
	  // Top
	      topR = rint * (Math.random() < 0.5 ? -1 : 1);
	}
  elmt.style.position = 'absolute'
  elmt.style.left = ((vpWidth - imageWidth)/2 + leftR) + 'px';
  elmt.style.top = ((vpHeight - imageHeight)/2 + topR + window.pageYOffset) + 'px';
}
