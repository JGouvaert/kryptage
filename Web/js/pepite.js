function init_pepites() {
    populate();
    var pps = document.getElementsByTagName("img");
    console.log(pps);
    for(var i=0; i<pps.length; i++) {
        center(pps[i], true);
        dragElement(pps[i])
    }

    cover();
}

function populate(){
  var nmb = Math.floor(Math.random() * 10) + 5,
      c = ["1", "2", "3"];
  for(var i=0; i < nmb; i++){
    var img = document.createElement("img");
    img.src = "img/Cookie" + c[i % c.length] + ".svg";
    img.id = "C" + (i + 1)%c.length;

    size(img, true, 45, 20);
    rotate(img, true, 360)

    var src = document.getElementById("cookies");
    src.appendChild(img);
  }
}

function cover(){
    var img = document.createElement("img"),
        src = document.getElementById("cookies");

    img.src = "img/Cookie2.svg";
    img.id = "cover";

    dragElement(img);

    var src = document.getElementById("cookies");
    src.appendChild(img);
}

function rotate(elmt, random, max){
  var rint = 0
  if(random){rint = Math.floor(Math.random() * (max + 1))}
  elmt.rotate = (0 + rint) + "deg";
}

function size(elmt, random, max, min){
  var rint = 0;
  if(random){
    rint = (Math.floor(Math.random() * (max + 1)));
  }
  elmt.height = elmt.width = (min + rint)+ "vh";
}
